import React, { Component } from "react";
import icon from "./svg/heart-solid.svg";

const Image = (props) => {
  return <img className={`${props.alt}`} src={props.src} alt={props.alt} />;
};

export default class MessageItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: this.props.text,
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.setState({ text: event.target.value });
  }

  render() {
    const { userId, text, date, avatar, id } = this.props;

    let dateParse = new Date(Date.parse(date));
    dateParse = `${dateParse.getHours() - 3}:${dateParse.getMinutes()}`;

    if (avatar === "#") {
      return (
        <div className="messageItem rightSide">
          <input
            className="textRightSide"
            type="text"
            id={userId}
            value={this.state.text}
            onChange={this.handleChange}
          />

          <input
            type="submit"
            value="Update"
            onClick={(e) => this.props.onUpdate(id, this.state.text)}
          />
          <input
            type="submit"
            value="Delete"
            onClick={(e) => this.props.onDelete(id)}
          />

          <div className="dateMessage">{dateParse}</div>
        </div>
      );
    } else {
      return (
        <div className="messageItem leftSide">
          <Image src={`${avatar}`} alt="avatar" />
          <img
            src={icon}
            className="like"
            alt="like"
            onClick={(e) => this.props.onLike(id)}
          />
          <div className="textMessage">{text}</div>
          <div className="comteinerModalForm">
            <button
              className="btn display-none"
              onClick={(e) => this.props.onEdit(id)}
              style={{ margin: "5px" }}
            >
              UPDATE
            </button>
          </div>
          <div className="dateMessage">{dateParse}</div>
        </div>
      );
    }
  }
}
