import {
  FETCH_MESAAGES_REQUEST,
  FETCH_MESAAGES_SUCCESS,
  FETCH_MESAAGES_FAILURE,
  DELETE_MESSAGE,
  UPDATE_MESSAGE,
  ADD_MESSAGE,
  LIKE_MESSAGE
} from "./actionTypes";

export const deleteMessage = (id) => ({
  type: DELETE_MESSAGE,
  payload: {
    id,
  },
});

export const updateMessage = (id, data) => ({
  type: UPDATE_MESSAGE,
  payload: {
    id,
    data,
  },
});

export const addMessage = (id, data) => ({
  type: ADD_MESSAGE,
  payload: {
    id,
    data,
  },
});

export const likeMessage = (id) => ({
  type: LIKE_MESSAGE,
  payload: {
    id
  },
})

export function fetchMessages() {
  return (dispatch) => {
    //   dispatch(getMessagesFetchRequest());
    return fetch("https://api.npoint.io/b919cb46edac4c74d0a8")
      .then(handleErrors)
      .then((res) => res.json())
      .then((res) => {
        console.log(res);
        dispatch(getMessagesFetchSuccess(res));
        return res;
      })
      .catch((error) => dispatch(getMessagesFetchFailure(error)));
  };
}

// Handle HTTP errors since fetch won't.
function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}
export const getMessagesFetchRequest = () => ({
  type: FETCH_MESAAGES_REQUEST,
});

export const getMessagesFetchSuccess = (messages) => ({
  type: FETCH_MESAAGES_SUCCESS,
  payload: { messages },
});

export const getMessagesFetchFailure = (error) => ({
  type: FETCH_MESAAGES_FAILURE,
  payload: { error },
});
