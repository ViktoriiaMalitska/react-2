import React, { Component } from "react";
import { connect } from 'react-redux';
import Message from './Message';
import * as actions from './actions';
import { USER_ID } from '../config';
import { setCurrentUserId, showPage } from '../updateMessage/actions';
// import { checkIsLike } from "./service";


    class MessageList extends Component {
        constructor(props) {
            super(props);
            this.onEdit = this.onEdit.bind(this);
            this.onUpdate = this.onUpdate.bind(this);
            this.onDelete = this.onDelete.bind(this);
            this.onAdd = this.onAdd.bind(this);
            this.onLike = this.onLike.bind(this);
        }
    
        onEdit(id) {
          console.log("klik")
          this.props.setCurrentUserId(id);
          this.props.showPage();
        }

        onUpdate(id, data) {
            this.props.updateMessage(id, data);
        }
    
        onDelete(id) {
            this.props.deleteMessage(id);
        }
    
        onAdd() {
            this.props.addMessage();
        }

        onLike(id) {
          this.props.likeMessage(id)
        }

        componentDidMount() {
          actions.fetchMessages();
          }
        
        render() {
            
            
            if (this.props.error) {
                return <div>Error! {this.props.error.message}</div>;
              }
          
              if (this.props.loading) {
                return <div>Loading...</div>;
              }
            return (
                
                <div className="messagesContainer">
                    
                        {
                            this.props.messages.map(message => {
                          
                                return (
                                    <Message
              
                                    key={message.id}
                                        id={message.id}
                                        userId={message.id}
                                        text={message.text}
                                        avatar={message.userId === USER_ID ? "#" : message.avatar}
                                        date={message.createdAt}
                                        onEdit={this.onEdit}
                                        onUpdate={this.onUpdate}
                                        onDelete={this.onDelete}
                                        onLike={this.onLike}
                                    />
                                );
                            })
                        }
                 
                    
                </div>
            );
        }
    }
    
    const mapStateToProps = (state) => {
      return {
        messages: state.messages.messages,
        loading: state.messages.loading,
        userId: state.messages.userId,
        likesMessage: state.messages.likesMessage
      }
    };
    
    const mapDispatchToProps = {
      ...actions,
      setCurrentUserId,
      showPage
    };
    
    export default connect(mapStateToProps,mapDispatchToProps )(MessageList);





