import { combineReducers } from "redux";
import messages from "../messages/reducer";
import updateForm from "../updateMessage/reducer";

const rootReducer = combineReducers({
    messages,
    updateForm
});

export default rootReducer;