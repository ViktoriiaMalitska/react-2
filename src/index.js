import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Chat from './Chat';
import { Provider } from "react-redux";
import store from "./store";
import './index.css';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  <Provider store={store}>
    <Chat/>
  </Provider>,
  document.getElementById('root')
);


serviceWorker.unregister();
