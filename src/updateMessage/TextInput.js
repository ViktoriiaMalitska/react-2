import React from 'react';


class TextInput extends React.Component {
    render() {
        const props = this.props;
        return (
            <div className="form-group row">
                <input
                    value={ props.text }
                    type={ props.type }
                    onChange={ e => props.onChange(e) }
                />
            </div>
        );
    }
}



export default TextInput;