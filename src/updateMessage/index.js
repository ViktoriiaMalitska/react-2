import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "./actions";
import { updateMessage } from "../messages/actions";
import TextInput from "./TextInput";

class UpdatePage extends Component {
  constructor(props) {
    super(props);
    this.onCancel = this.onCancel.bind(this);
    this.onSave = this.onSave.bind(this);
    this.onChangeData = this.onChangeData.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.currentMessageId !== this.props.currentMessageId) {
      const message = this.props.messages.find(
        (message) => message.id === nextProps.currentMessageId
      );
      this.setState(message);
    }
  }

  /*
    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.userId !== prevState.userId) {
            const user = nextProps.users.find(user => user.id === nextProps.userId);
            return {
                ...user
            };
        }
    }
    */

  onCancel() {
    this.props.dropCurrentUserId();
    this.props.hidePage();
    this.setState({});
  }

  onSave() {
    this.props.updateMessage(this.props.currentMessageId, this.state.text);
    this.props.dropCurrentUserId();
    this.props.hidePage();
  }

  onChangeData(e) {
    const value = e.target.value;
    this.setState({
      text: value,
    });
  }

  getUserPageContent() {
    // const isValid = this.refs.email ? this.refs.email.getValidationStatus() : true;

    return (
      <div
        className="modal"
        style={{ display: "block" }}
        tabIndex="-1"
        role="dialog"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content" style={{ padding: "5px" }}>
            <div className="modal-body">
              <TextInput text={this.state.text} onChange={this.onChangeData} />
            </div>
            <div className="modal-footer">
              <button className="btn btn-secondary" onClick={this.onCancel}>
                Cancel
              </button>
              <button className="btn btn-primary" onClick={this.onSave}>
                Save
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const isShown = this.props.isShown;
    return isShown ? this.getUserPageContent() : null;
  }
}

const mapStateToProps = (state) => {
  return {
    messages: state.messages.messages,
    isShown: state.updateForm.isShown,
    currentMessageId: state.updateForm.currentMessageId,
  };
};

const mapDispatchToProps = {
  ...actions,
  updateMessage,
};

export default connect(mapStateToProps, mapDispatchToProps)(UpdatePage);
