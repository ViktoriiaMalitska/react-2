import React, { Component } from "react";
import { USER_ID } from "../../config";
import { connect } from "react-redux";
import {addMessage} from "../../messages/actions"

class NewMessageForm extends React.Component {
    constructor() {
      super()
      this.state = {
        message: ''
      }
      this.handleChange = this.handleChange.bind(this)
      this.onSave = this.onSave.bind(this);
    }

    handleChange(e) {
      this.setState({
        message: e.target.value
      })
    }

    onSave() {
      this.props.addMessage(USER_ID, this.state.message)
      this.setState({
        message: ''
      })
    }
    render() {
      return (
        <div
          className="sendMessageForm">
          <input
          className="sendMessageForm "
            onChange={this.handleChange}
            value={this.state.message}
            placeholder="Message"
            type="text" />
            <button type="submit" onClick={this.onSave}>{`Send`}</button>
        </div>
      )
    }
  }
  const mapStateToProps = (state) => {
    return {
      messages: state.messages.messages,
    }
};
  const mapDispatchToProps = {
    addMessage
};
export default connect(mapStateToProps, mapDispatchToProps)(NewMessageForm);
