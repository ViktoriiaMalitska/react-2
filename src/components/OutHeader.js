import React from "react";
import logo from "../messages/svg/headset-solid.svg";

const OutHeader = () => {
      return (
        <header className="outHeaderContainer">
        <img src={logo} className="logo" alt="logo" />
          <div>{`LOGO`}</div>
        </header>
      );
  };
  
  export default OutHeader;