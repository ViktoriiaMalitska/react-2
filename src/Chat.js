import React from 'react';
import MessageList from './messages/index';
import Header from "./components/Header/Header.js";
import OutHeader from "./components/OutHeader";
import NewMessageForm from "./components/createMessage";
import Footer from "./components/Footer";
import UpdatePage from './updateMessage';
import './App.css';

function Chat() {
  return (
    <div className="mainContainer">
      <OutHeader/>
      <Header/>
      <UpdatePage/>
      <MessageList />
      <NewMessageForm/>
      <Footer/>
    </div>
  );
}

export default Chat;